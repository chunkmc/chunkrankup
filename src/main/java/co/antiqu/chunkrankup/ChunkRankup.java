package co.antiqu.chunkrankup;

import co.antiqu.chunkrankup.util.Config;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

public final class ChunkRankup extends JavaPlugin {

    @Getter
    private static ChunkRankup instance;

    @Getter
    private Config lang;

    @Override
    public void onEnable() {
        instance = this;
        setUp();
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    private void setUp() {
        config();
        listeners();
        commands();
        objects();
    }

    private void config() {
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
        this.lang = new Config(this,"lang.yml");
        this.lang.saveDefaultConfig(this);
    }

    private void listeners() {

    }

    private void commands() {

    }

    private void objects() {

    }
}
