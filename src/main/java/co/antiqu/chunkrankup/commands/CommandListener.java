package co.antiqu.chunkrankup.commands;

import co.antiqu.chunkrankup.ChunkRankup;
import co.antiqu.chunkrankup.commands.cmd.CMDRankup;
import co.antiqu.chunkrankup.util.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class CommandListener implements CommandExecutor {

    protected ChunkRankup instance;

    private HashMap<String,FCommand> commandList;

    public CommandListener(ChunkRankup instance) {
            this.instance = instance;
            commandList = new HashMap<>();

            commandList.put("rankup",new CMDRankup());

        commandList.keySet().forEach(n -> {
            instance.getCommand(n).setExecutor(this);
        });
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(commandList.containsKey(cmd.getName())) {
            FCommand command = commandList.get(cmd.getName());

            if(!(sender instanceof Player))
                return false;

            command.setPlayerSender((Player) sender);

            if(!sender.hasPermission(command.getPermission().getPerm())) {
                sender.sendMessage(Lang.NO_PERMISSION);
                return false;
            }

            if(args.length != command.getLength()) {
                sender.sendMessage(Lang.INCORRECT_USAGE);
                return false;
            }

            command.run(sender,args);
        }
        return false;
    }
}
