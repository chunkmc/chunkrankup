package co.antiqu.chunkrankup.commands;

import co.antiqu.chunkrankup.ChunkRankup;
import co.antiqu.chunkrankup.util.Permission;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class FCommand {

    @Getter @Setter
    private String command;
    @Getter @Setter
    private Permission permission;
    @Getter @Setter
    private String usage;

    @Getter @Setter
    private boolean console;
    @Getter @Setter
    private boolean player;

    @Getter @Setter
    private Player playerSender;

    @Getter @Setter
    private int length;

    public boolean isBoth() {
        return console && player;
    }

    public abstract void run(CommandSender sender, String[] args);


}
