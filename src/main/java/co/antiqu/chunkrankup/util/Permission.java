package co.antiqu.chunkrankup.util;

import lombok.Getter;

public enum Permission {

    RANKUP("rankup.rankup"),
    NO_PERMISSION("");

    @Getter
    private String perm;

    Permission(String s) {
        this.perm = s;
    }

}
