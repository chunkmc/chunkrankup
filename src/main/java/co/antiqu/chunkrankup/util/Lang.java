package co.antiqu.chunkrankup.util;

import co.antiqu.chunkrankup.ChunkRankup;
import org.bukkit.configuration.file.FileConfiguration;

public class Lang {

    private static FileConfiguration lang = ChunkRankup.getInstance().getLang().getConfig();

    public static String NO_PERMISSION;
    public static String INCORRECT_USAGE;


    public Lang() {
        NO_PERMISSION = Color.t(lang.getString("no_permission"));
        INCORRECT_USAGE = Color.t(lang.getString("incorrect_usage"));
    }

}
